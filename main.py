from flask_orator import jsonify
from flask import Flask, render_template, send_from_directory, request
from app.models import Asistencia as asis
from app import db_config
from app.models import Empleados as EM
from app.models import Horarios as HR
from flask import Flask
from flask_orator import Orator
import json
import time
import datetime
app = Flask(__name__, static_url_path= '')
app.config['ORATOR_DATABASES']= db_config.db_config
db= Orator(app)

def restar_hora(hora1,hora2):
    formato = "%H:%M:%S"
    h1 = datetime.datetime.strptime(hora1, formato)
    h2 = datetime.datetime.strptime(hora2, formato)
    resultado = h1 - h2
    resultado = str(resultado)
    resultado = resultado.replace('-1 day, ','')
    return str(resultado)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/')
def home():
    return render_template('index.html')

@app.route('/css/<path:path>')
def send(path):
    return send_from_directory('css',path)

@app.route('/<path:path>')
def enviarsss(path):
    return send_from_directory('favicon',path)



def getJson(url):
    jsonFile = open(url)
    print(url)
    answer = json.load(jsonFile)
    print(answer)
    return answer



@app.route('/menu')
def menu ():
    return render_template('menu.html')


@app.route('/showDatabase')
def dataS():
    return jsonify(EM.get().serialize())


@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/Salida')
def salidaREF():
    return render_template('salida.html')


@app.route('/salida', methods=['POST'])
def salida():
    user= request.form['username']
    password= request.form['password']

    try:
        usuario= EM.where_user(user).where_password(password).first()
    except Exception as e:
        return render_template('passwordWrongOut.html')

    try:
        if usuario is None:
            return render_template('passwordWrongOut.html')
        else:
            id = db.table('empleados').where('user',user).first()
            exis = asis.where_ID_empleado(id['id']).first()
            if exis is None:
                return render_template('noSalida.html')
            else:
                tabla = asis.where_ID_empleado(id['id']).get()
                reverse = tabla.reverse()
                ultimo = reverse.take(1).serialize()
                ultimo = ultimo[0]

                if ultimo['salida'] == 1 and ultimo['entrada'] == 1:
                    return render_template('noSalida.html')
                elif ultimo['salida'] ==0 and ultimo['entrada'] ==1:
                    idD = ultimo['ID']
                    registro = asis.find(idD)

                    registro.salida = True

                    registro.hora_salida = restar_hora(time.strftime("%H:%M:%S"),"07:00:00")
                    registro.horas_total = restar_hora(str(restar_hora(time.strftime("%H:%M:%S"),"07:00:00")),str(ultimo['hora_entrada']))

                    registro.save()


                    return render_template('correctOut.html')


    except Exception as e:
        return jsonify({"error": str(e.args)})
    return render_template('salida.html')


@app.route('/Entrada')
def entradaREF():
    return render_template('entrada.html')

@app.route('/entrada', methods=['POST'])
def entrada():
    user= request.form['username']
    password= request.form['password']
    try:
        usuario= EM.where_user(user).where_password(password).first()
    except Exception as e:
        return render_template('passwordwrongIn.html')



    try:
        if usuario is None:
            return render_template('passwordwrongIn.html')
        else:
            id = db.table('empleados').where('user',user).first()
            exis = asis.where_ID_empleado(id['id']).first()
            if exis is None:
                asistencia = asis()
                asistencia.ID_empleado = id['id']
                asistencia.fecha = time.strftime("%y-%m-%d")
                asistencia.nombre = id['nombres']
                asistencia.entrada = True
                asistencia.hora_entrada = restar_hora(time.strftime("%H:%M:%S"),"07:00:00")
                asistencia.salida = False
                asistencia.hora_salida = "00:00:00"
                asistencia.save()
                return render_template('correctIn.html')
            else:
                tabla = asis.where_ID_empleado(id['id']).get()
                reverse = tabla.reverse()
                ultimo = reverse.take(1).serialize()
                ultimo = ultimo[0]

                if ultimo['entrada'] == 1 and ultimo['salida'] == 1:

                    asistencia = asis()
                    asistencia.ID_empleado = id['id']
                    asistencia.fecha = time.strftime("%y-%m-%d")
                    asistencia.nombre = id['nombres']
                    asistencia.entrada = True
                    asistencia.hora_entrada = restar_hora(time.strftime("%H:%M:%S"),"07:00:00")
                    asistencia.salida = False
                    asistencia.hora_salida = "00:00:00"
                    asistencia.horas_total= "00:00:00"
                    asistencia.save()
                    return render_template('correctIn.html')

                elif ultimo['entrada'] ==1 and ultimo['salida'] ==0:

                    return render_template('noEntrada.html')

    except Exception as e:
        return jsonify({"error": str(e)})
    return render_template('entrada.html')

@app.route('/Agregarempleado')
def adEmpleado():
    return render_template('agregarEmpleado.html')


@app.route('/AgregarEmpleado', methods=["POST"])
def agregarEmpleado():
    try:

        Empleados = EM()
        nom = request.form['nombre']
        a_pa= request.form['a_paterno']
        a_ma= request.form['a_materno']
        edad= request.form['edad']
        puesto= request.form['puesto']
        user = request.form['username']
        pas = request.form['password']


        Empleados.nombres= nom
        Empleados.a_paterno= a_pa
        Empleados.a_materno= a_ma
        Empleados.edad= edad
        Empleados.puesto= puesto
        Empleados.user= user
        Empleados.password= pas

        Empleados.save()
        return jsonify({'status': 'exitoso'})

    except Exception as e:
        print(str(e))
        return jsonify({'error': str(e)})
    return ""

#parte del menu de los reportes quincenales
@app.route('/menuReporte')
def reporte15na():
    return render_template('subMenuReporte.html')

@app.route('/newHorario',methods=['POST'])
def horario():
    try:
        content = request.get_json(force=True)
        ID_empleado=content['ID_empleado']
        nombre=content['nombre']
        hora_entrada_lu=content['hora_in_lu']
        hora_entrada_ma=content['hora_in_ma']
        hora_entrada_mi=content['hora_in_mi']
        hora_entrada_ju=content['hora_in_ju']
        hora_entrada_vi=content['hora_in_vi']
        hora_entrada_sa=content['hora_in_sa']
        hora_salida_lu=content['hora_out_lu']
        hora_salida_ma=content['hora_out_ma']
        hora_salida_mi=content['hora_out_mi']
        hora_salida_ju=content['hora_out_ju']
        hora_salida_vi=content['hora_out_vi']
        hora_salida_sa=content['hora_out_sa']
        horario_nuevo= HR()
        horario_nuevo.ID_empleado=ID_empleado
        horario_nuevo.nombre=nombre
        horario_nuevo.hora_entrada_lu=hora_entrada_lu
        horario_nuevo.hora_entrada_ma=hora_entrada_ma
        horario_nuevo.hora_entrada_mi=hora_entrada_mi
        horario_nuevo.hora_entrada_ju=hora_entrada_ju
        horario_nuevo.hora_entrada_vi=hora_entrada_vi
        horario_nuevo.hora_entrada_sa=hora_entrada_sa
        horario_nuevo.hora_salida_lu=hora_salida_lu
        horario_nuevo.hora_salida_ma=hora_salida_ma
        horario_nuevo.hora_salida_mi=hora_salida_mi
        horario_nuevo.hora_salida_ju=hora_salida_ju
        horario_nuevo.hora_salida_vi=hora_salida_vi
        horario_nuevo.hora_salida_sa=hora_salida_sa
        horario_nuevo.save()
        return "True"
    except Exception as e:
        return jsonify({"error":str(e)})



@app.route('/MenuReporte')
def promedioPuntualidad():
    return None


if __name__ == '__main__':
    app.run(debug=True)
