import os
db_config_local = {
    'development': {
        'driver': 'mysql',
        'database': 'MarzhalAsistencia',
        'host':'127.0.0.1',
        'user': 'root',
        'password': 'Asistencia7789',
        'prefix': '',
        'charset':'utf8'
    }
}
db_config_production = {
    'development': {
        'driver': 'mysql',
        'database': 'MarzhalAsistencia',
        'user': 'root',
        'password': 'Asistencia7789',
        'prefix': '',
        'unix_socket': '/cloudsql/marzhal-asistencia:us-east4:asistencia',
        'charset':'utf8'
    }
}

if os.environ.get('FLASK_ENV',False)=='production':
    db_config = db_config_production
else:
    db_config = db_config_local
