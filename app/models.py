from orator import Model
class Asistencia(Model):
    __table__ = 'asistencia'
    __primary_key__ = 'ID'


class Empleados(Model):
    __table__ = 'empleados'
    __hidden__ =['user','password','created_at', 'updated_at']

class Horarios(Model):
    __table__: 'horarios'
    __primary_key__ = 'ID'
