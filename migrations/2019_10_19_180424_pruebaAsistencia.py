from orator.migrations import Migration


class PruebaAsistencia(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('asistencia_test') as table:
            table.increments('ID')
            table.integer('ID_empleado').unsigned()
            table.foreign('ID_empleado').references('id').on('empleados')
            table.date('fecha')
            table.string('nombre')
            table.boolean('entrada')
            table.time('hora_entrada')
            table.boolean('salida')
            table.time('hora_salida')

            table.timestamps()


    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('asistencia_test')
