from orator.migrations import Migration


class Altethorarios(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.table('asistencia') as table:
            table.time('horas_total')

    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('asistencia') as table:
            table.drop_column('horas_total')
