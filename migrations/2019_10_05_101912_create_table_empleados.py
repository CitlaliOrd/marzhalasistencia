from orator.migrations import Migration


class CreateTableEmpleados(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('empleados') as table:
            table.increments('id')
            table.string('a_paterno')
            table.string('a_materno')
            table.string('nombres')
            table.integer('edad')
            table.string('puesto')
            table.string('user')
            table.string('password')
            table.timestamps()


    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('empleados')
