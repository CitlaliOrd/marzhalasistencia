from orator.migrations import Migration


class CrearHorarios(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('horarios') as table:
            table.increments('ID')
            table.integer('ID_empleado').unsigned()
            table.foreign('ID_empleado').references('id').on('empleados')
            table.string('nombre')
            table.time('hora_entrada_lu')
            table.time('hora_entrada_ma')
            table.time('hora_entrada_mi')
            table.time('hora_entrada_ju')
            table.time('hora_entrada_vi')
            table.time('hora_entrada_sa')
            table.time('hora_salida_lu')
            table.time('hora_salida_ma')
            table.time('hora_salida_mi')
            table.time('hora_salida_ju')
            table.time('hora_salida_vi')
            table.time('hora_salida_sa')


            table.timestamps()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('horarios')
