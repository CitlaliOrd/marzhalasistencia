from orator.migrations import Migration


class CreateTableAsistencia(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('asistencia') as table:
            table.increments('ID')
            table.date('fecha')
            table.string('nombre')
            table.boolean('entrada')
            table.time('hora_entrada')
            table.boolean('salida')
            table.time('hora_salida')

            table.timestamps()


    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('asistencia')
